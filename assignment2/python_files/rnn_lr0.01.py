import codecs
import json
import matplotlib.pyplot as plt
import math
import matplotlib.ticker as ticker
import numpy as np
import os
import random
import time
from time import sleep
import subprocess
import re

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F
import torch.utils.data

torch.set_num_threads(1)

MAX_LENGTH = 50

class EncoderRNN(nn.Module):
    def __init__(self, input_size, hidden_size, dropout_p=0.1):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.dropout_p = dropout_p

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.dropout = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(hidden_size, hidden_size)


    def forward(self, input, hidden):
        word_embedded = self.embedding(input).view(1, 1, -1)
        word_embedded = self.dropout(word_embedded)
        output, hidden = self.gru(word_embedded, hidden)
        return output, hidden


    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size)


class EncoderPOS(nn.Module):
    def __init__(self, input_size, hidden_size, dropout_p=0.1):
        super(EncoderPOS, self).__init__()
        self.hidden_size = hidden_size
        self.dropout_p = dropout_p

        self.word_embedding = nn.Embedding(input_size, hidden_size)
        self.pos_embedding = nn.Embedding(MAX_LENGTH, hidden_size)
        self.dropout = nn.Dropout(self.dropout_p)
        self.linear = nn.Linear(hidden_size*2, hidden_size)

    def forward(self, input, hidden):
        index = input[0]
        word = input[1]
        word_embedded = self.word_embedding(word).view(1, 1, -1)
        pos_embedded = self.pos_embedding(torch.tensor([index], dtype=torch.long)).view(1, 1, -1)

        combined_embedding = torch.cat((word_embedded.squeeze(0), pos_embedded.squeeze(0)), 1)
        output = self.dropout(combined_embedding)
        output = self.linear(output)
        return output, hidden


    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size)



class AttnDecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, dropout_p=0.1, max_length=MAX_LENGTH):
        super(AttnDecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.dropout_p = dropout_p
        self.max_length = max_length

        self.embedding = nn.Embedding(self.output_size, self.hidden_size)
        self.attn = nn.Linear(self.hidden_size * 2, self.max_length)
        self.attn_combine = nn.Linear(self.hidden_size * 2, self.hidden_size)
        self.dropout = nn.Dropout(self.dropout_p)
        self.gru = nn.GRU(self.hidden_size, self.hidden_size)
        self.out = nn.Linear(self.hidden_size, self.output_size)


    def forward(self, input, hidden, encoder_outputs):
        embedded = self.embedding(input).view(1, 1, -1)
        embedded = self.dropout(embedded)

        attn_weights = F.softmax(
            self.attn(torch.cat((embedded[0], hidden[0]), 1)), dim=1)
        attn_applied = torch.bmm(attn_weights.unsqueeze(0),
                                 encoder_outputs.unsqueeze(0))

        output = torch.cat((embedded[0], attn_applied[0]), 1)
        output = self.attn_combine(output).unsqueeze(0)

        output = F.relu(output)
        output, hidden = self.gru(output, hidden)

        output = F.log_softmax(self.out(output[0]), dim=1)
        return output, hidden, attn_weights


    def initHidden(self):
        return torch.zeros(1, 1, self.hidden_size)


class NMT(nn.Module):
    def __init__(self, i2w_english, i2w_french, hidden_size, positional, criterion):
        super(NMT, self).__init__()
        if positional:
            self.encoder = EncoderPOS(len(i2w_french), hidden_size)
        else:
            self.encoder = EncoderRNN(len(i2w_french), hidden_size)
        self.decoder = AttnDecoderRNN(hidden_size, len(i2w_english))
        self.positional = positional
        self.criterion = criterion


    def forward(self, input_tensor, target_tensor):
        loss = 0

        input_length = input_tensor.size(0)
        target_length = target_tensor.size(0)

        encoder_hidden = self.encoder.initHidden()
        encoder_outputs = torch.zeros(MAX_LENGTH, self.encoder.hidden_size)

        if input_length > MAX_LENGTH: input_length = MAX_LENGTH

        if self.positional:
            average_hidden = torch.zeros(1, self.encoder.hidden_size)

            for ei in range(input_length):
                encoder_output, encoder_hidden = self.encoder((ei, input_tensor[ei]), encoder_hidden)
                average_hidden += encoder_output
                encoder_outputs[ei] = encoder_output[0, 0]
            encoder_hidden = (average_hidden/input_length).unsqueeze(0)
        else:
            for ei in range(input_length):
                encoder_output, encoder_hidden = self.encoder(input_tensor[ei], encoder_hidden)
                encoder_outputs[ei] = encoder_output[0, 0]

        decoder_input = torch.tensor([[SOS_token_en]])
        decoder_hidden = encoder_hidden

        # Use teacher forcing
        for di in range(target_length):
            decoder_output, decoder_hidden, decoder_attention = self.decoder(
                decoder_input, decoder_hidden, encoder_outputs)
            loss += self.criterion(decoder_output, target_tensor[di])
            decoder_input = target_tensor[di]

        loss.backward()

        return loss.item()/target_length


    def evaluate(self, w2i_french, i2w_english, input_sentence, target_sentence):
        with torch.no_grad():
            input_tensor = sentence_to_tensor(w2i_french, input_sentence)
            target_tensor = sentence_to_tensor(w2i_english, target_sentence)

            input_length = input_tensor.size()[0]
            encoder_hidden = self.encoder.initHidden()

            encoder_outputs = torch.zeros(MAX_LENGTH, self.encoder.hidden_size)

            if input_length > MAX_LENGTH: input_length = MAX_LENGTH

            if self.positional:
                average_hidden = torch.zeros(1, self.encoder.hidden_size)

                for ei in range(input_length):
                    encoder_output, encoder_hidden = self.encoder((ei, input_tensor[ei]), encoder_hidden)
                    average_hidden += encoder_output
                    encoder_outputs[ei] += encoder_output[0, 0]
                encoder_hidden = (average_hidden/input_length).unsqueeze(0)
            else:
                for ei in range(input_length):
                    encoder_output, encoder_hidden = self.encoder(input_tensor[ei], encoder_hidden)
                    encoder_outputs[ei] += encoder_output[0, 0]

            decoder_input = torch.tensor([[SOS_token_en]])  # SOS

            decoder_hidden = encoder_hidden

            decoded_words = []
            decoder_attentions = torch.zeros(MAX_LENGTH, MAX_LENGTH)

            for di in range(MAX_LENGTH):
                decoder_output, decoder_hidden, decoder_attention = self.decoder(
                    decoder_input, decoder_hidden, encoder_outputs)
                decoder_attentions[di] = decoder_attention.data
                topv, topi = decoder_output.data.topk(1)
                if topi.item() == EOS_token_en:
                    decoded_words.append("<eos>")
                    break
                else:
                    decoded_words.append(i2w_english[topi.item()])

                decoder_input = topi.squeeze().detach()
        return decoded_words, decoder_attentions[:di + 1]


def read_data(file_name):
    """
    Reads the data and returns it in a list.
    """

    f = codecs.open(file_name, "r", encoding='utf-8')
    return [line.strip().split() for line in f.readlines()]


def word_to_index(file_name):
    """
    Obtains the vocabulary of a file and returns it
    in a dictionary to be able to use w2i.
    """

    file = codecs.open(file_name, "r", encoding='utf-8')
    w2i = json.load(file)
    w2i["<sos>"] = len(w2i)
    return w2i


def index_to_word(dictionary):
    """
    Reverses the dictionary such that i2w can be used.
    """

    reversed_dict = {}

    for word, index in dictionary.items():
        reversed_dict[index] = word
    return reversed_dict


def sentence_to_indices(w2i, sentence):
    """
    Returns the indices of the words in a sentence in a list.
    """

    return [w2i[word] for word in sentence]


def sentence_to_tensor(w2i, sentence):
    """
    Returns the tensor of a sentence.
    """

    indices = sentence_to_indices(w2i, sentence)
    indices.append(w2i["<eos>"])
    return torch.tensor(indices, dtype=torch.long).view(-1, 1)


def train(input_sentence, target_sentence, w2i_english,
          w2i_french, nmt, nmt_optimizer, criterion, max_length=MAX_LENGTH):
    """
    Does one iteration of training.
    """

    loss = 0
    output_sentence = []
    input_tensor = sentence_to_tensor(w2i_french, input_sentence)
    target_tensor = sentence_to_tensor(w2i_english, target_sentence)
    target_length = target_tensor.size(0)

    nmt_optimizer.zero_grad()
    loss = nmt.forward(input_tensor, target_tensor)

    nmt_optimizer.step()

    return loss


def train_dataset(w2i_english, w2i_french, train_english,
                  train_french, nmt, learning_rate):
    """
    Trains the Encoder-Decoder model for the entire data set.
    """

    start = time.time()

    nmt_optimizer = optim.SGD(nmt.parameters(), lr=learning_rate)
    criterion = nn.NLLLoss()
    total_loss = 0

    for iter in range(1, len(train_english) + 1):
        input_sentence = train_french[iter-1]
        target_sentence = train_english[iter-1]
        loss = train(input_sentence, target_sentence, w2i_english,
                     w2i_french, nmt, nmt_optimizer, criterion)
        total_loss += loss
    return total_loss


def evaluate_dataset(w2i_french, i2w_english, nmt, val_french, val_english):
    """
    Evaluates the data set and returns the obtained predictions and loss.
    """

    predictions = []

    for i, input_sentence in enumerate(val_french):
        target_sentence = val_english[i]

        predicted_words, attentions = nmt.evaluate(w2i_french, i2w_english,
                                                   input_sentence, target_sentence)
        predicted_sentence = ' '.join(predicted_words)
        predictions.append(predicted_sentence)
    return predictions


def save_model(model, positional, epoch, lr, train_loss, val_score):
    """
    Saves the NMT model to a file.
    """

    model_type = "rnn"
    if positional: model_type = "pos"

    path = "models/" + model_type + "_lr" + str(lr) + "_epoch" + str(epoch) + \
        "_trainloss" + str(train_loss) + "_valscore" + str(val_score)
    torch.save(model.state_dict(), path)


def save_predictions(predictions, epoch, lr, train_loss, positional):
    """
    Saves the encoded predicted sentences decoded to a file.
    """

    model_type = "rnn"
    if positional: model_type = "pos"

    path = "results/predictions_" + model_type + "_lr" + str(lr) + \
        "_epoch" + str(epoch) + "_trainloss" + str(train_loss) + ".txt"

    with codecs.open("temp_encoded.txt", "w", encoding='utf-8') as f:
        for sentence in predictions:
            f.write(sentence.split("<")[0] + "\n")
    f.close()

    command = "sed -r 's/(@@ )|(@@ ?$)//g' temp_encoded.txt > " + path
    os.system(command)
    os.remove("temp_encoded.txt")
    return path


def evaluate_predictions(predictions_path, val_path):
    """
    Computes BLEU score for predictions file.
    """
    command = " ".join(["perl", "multi-bleu.perl", val_path, "<", predictions_path])

    try:
        BLEU_full = "".join(re.split(r'BLEU = |\\n\'', str(subprocess.check_output(command, shell=True)))[1:])
        BLEU_score = re.split(r',', BLEU_full)[0].strip()
    except:
        BLEU_full = "error"
        BLEU_score = None

    return BLEU_score, BLEU_full


def save_losses(train_loss, val_score, learning_rate):
    """
    Appends the obtained losses to a file.
    """

    model_type = "rnn"
    if positional: model_type = "pos"
    path = model_type + "_lr" + str(learning_rate)

    with codecs.open("results/trainlosses_" + path + ".txt", "a", encoding='utf-8') as train_file:
        train_file.write(str(train_loss) + "\n")
    train_file.close()

    with codecs.open("results/valscores_" + path + ".txt", "a", encoding='utf-8') as val_file:
        val_file.write(str(val_score) + "\n")
    val_file.close()


def train_and_evaluate(w2i_english, w2i_french, train_english, train_french,
                       nmt, positional, num_epochs, learning_rate=0.01):
    """
    Trains the Encoder-Decoder for a certain amount of epochs.
    """

    train_losses = []
    val_scores = []

    for iter in range(1, num_epochs + 1):
        print("Iteration", iter, "of", num_epochs)
        train_loss = train_dataset(w2i_english, w2i_french,
                                   train_english, train_french,
                                   nmt, learning_rate)
        predictions = evaluate_dataset(w2i_french, i2w_english,
                                       nmt, val_french, val_english)

        predictions_path = save_predictions(predictions, iter, learning_rate, train_loss,
                                            positional)

        sleep(0.01)

        val_score, bleu_output = evaluate_predictions(predictions_path, "val/val.en")

        print("training loss:", train_loss)
        print("validation bleu:", bleu_output)
        train_losses.append(train_loss)
        val_scores.append(val_score)

        save_model(nmt, positional, iter, learning_rate, train_loss, val_score)
        save_losses(train_loss, val_score, learning_rate)


if __name__ == "__main__":
    train_english = read_data("data/train_preprocessed.en")
    train_french = read_data("data/train_preprocessed.fr")

    val_english = read_data("data/val_preprocessed.en")
    val_french = read_data("data/val_preprocessed.fr")

    w2i_french = word_to_index("data/train_preprocessed.fr.json")
    w2i_english = word_to_index("data/train_preprocessed.en.json")

    i2w_french = index_to_word(w2i_french)
    i2w_english = index_to_word(w2i_english)

    EOS_token_en = w2i_english["<eos>"]
    SOS_token_en = w2i_english["<sos>"]

    EOS_token_fr = w2i_french["<eos>"]
    SOS_token_fr = w2i_french["<sos>"]

    positional = False
    num_epochs = 10
    learning_rate = 0.01

    criterion = nn.NLLLoss()
    nmt = NMT(i2w_english, i2w_french, 256, positional, criterion)
    train_and_evaluate(w2i_english, w2i_french, train_english,
                       train_french, nmt, positional, num_epochs,
                       learning_rate)
